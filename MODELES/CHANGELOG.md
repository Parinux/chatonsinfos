# ChatonsInfos Changelog

All notable changes of ChatonsInfos will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Added
### Changed
- host.description : création des fichiers properties
### Deprecated
### Removed
### Fixed


## [0.1] - 2020-12-17
### Added
- all
### Changed
### Deprecated
### Removed
### Fixed

